<?php
class Validation
{
	protected $data = [];
	
	protected $signup_validators = [	'formFullName' => 'isFullNameValid',
									'formEmail' => 'isEmailValid',
									'formPassword' => 'isPasswordValid',
									'formRetype' => 'isRetypeValid',
									];
	
	protected $login_validators = [ 	'LoginEmail' =>'isLoginEmailValid',
										'LoginPassword' => 'isLoginPasswordValid',
									];
						
	
	protected $errors = [];
	
	
	
	public function __construct(array $PostData)
	{
		if(empty($PostData))
		{
			trigger_error('Validation was triggered with an empty data set', E_USER_WARNING);
		}
		
		$this->data = $PostData;
	}
	
	
	public function signupValidate() : bool
	{
		if(empty($this->data))
		{
			trigger_error('No data was inputted for validation.', E_USER_WARNING);
			return true;
		}
		
		
		foreach($this->data as $key => $value)
		{
			if (!array_key_exists($key, $this->signup_validators)) 
			{
                $warning_msg = 'Form field ' . $key . ' has no registered validator method. Validation not done';
                trigger_error($warning_msg, E_USER_WARNING);
                
            }
			
			else
			{
				$validator = $this->signup_validators[$key];
				
				if(is_null($validator))
				{
					continue;
				}
				
				if(!method_exists($this, $validator))
				{
					$warning ='' . $key . ' has a registered validator but no corresponding validator method. Validation not done';
                    trigger_error($warning, E_USER_WARNING);
				}
				
				else
				{
					$this->$validator($value);
				}
			}
		}	
		return(empty($this->errors) ? true : false);
	}
	
	
	public function loginValidate() : bool
	{
		if(empty($this->data))
		{
			trigger_error('No data was inputted for validation.', E_USER_WARNING);
			return true;
		}
		
		
		foreach($this->data as $key => $value)
		{
			if (!array_key_exists($key, $this->login_validators)) 
			{
                $warning_msg = 'Form field ' . $key . ' has no registered validator method. Validation not done';
                trigger_error($warning_msg, E_USER_WARNING);
                
            }
			
			else
			{
				$validator = $this->login_validators[$key];
				
				if(is_null($validator))
				{
					continue;
				}
				
				if(!method_exists($this, $validator))
				{
					$warning ='' . $key . ' has a registered validator but no corresponding validator method. Validation not done';
                    trigger_error($warning, E_USER_WARNING);
				}
				
				else
				{
					$this->$validator($value);
				}
			}
		}	
		return(empty($this->errors) ? true : false);
	}
	
	
	public function getErrors() : array
	{
		return $this->errors;
	}
	
	
	protected function setErrors(string $FieldName, string $ErrorMessage)
	{
		if(empty($ErrorMessage))
		{
			trigger_error('Error Message could not be generated', E_USER_ERROR);
		}
		
		if(empty($FieldName))
		{
			trigger_error('Field cannot be empty.', E_USER_ERROR);
		}
		
		$this->errors[$FieldName] = $ErrorMessage;
	}
	
	
	protected function isFullNameValid(string $fullName) : bool
	{
		if(empty($fullName))
		{
			echo "<br>";
			//echo "ERROR! Please enter Your Name!";
			echo "<br>";
			
			$this->setErrors('formFullName', 'ERROR: Please enter Your Name!');
			return false;
		}
		
		$fullname = explode(" ", $fullName);
	
		if(empty($fullname[0]) || empty($fullname[1]))
		{
			//echo "ERROR! Invalid Full Name!";
			echo "<br>";
			echo "<br>";
			echo "<br>";
			
			$this->setErrors('formFullName', 'ERROR: Please enter your First AND Last Name!');
			
			return false;
		}

		else
		{
			//echo "Valid Full Name!";
			echo "<br>";
			echo "<br>";
			echo "<br>";
			return true;
		}
	}
	
	
	protected function isEmailValid(string $email) : bool
	{
		if(empty($email))
		{
			//echo "ERROR! Email is required!";
			
			$this->setErrors('formEmail', 'ERROR: Please enter Your Email Address!');
			
			return false;
		}
		
		else;
		
		//Removing any illegal/unauthorized characters from the email
		//$email = filter_var($email, FILTER_SANITIZE_EMAIL);
		
		if(!filter_var($email, FILTER_VALIDATE_EMAIL))
		{
			//echo "ERROR! Invalid Email Address!";
			
			$this->setErrors('formEmail', 'ERROR: Please enter a VALID Email Address!');
			
			return false;
		}
		
		else;
		
		//echo "Valid Email Address";
		return true;
	}
	
	
	protected function isPasswordValid(string $password) : bool
	{
		if(empty($password))
		{
			echo "<br>";
			//echo "ERROR! Please enter a password";
			echo "<br>";
			$this->setErrors('formPassword', 'ERROR: Please enter a password!');
			
			return false;
		}
		
		if(strlen($password) < 10)
		{
			echo "<br>";
			//echo "ERROR! Password must be at least 10 Characters long";
			echo "<br>";
			
			$this->setErrors('formPassword', 'ERROR: Your password must be at least 10 characters long!');
			
			return false;
		}
		
		$count = strlen($password);
		
		for($x = 0; $x < $count; $x++)
		{
			if(preg_match("/[A-Z]/", $password)===0) 
			{
			echo "<br>";				
			//echo "ERROR! Password requires an Upper Case Character!";
			echo "<br>";
			
			$this->setErrors('formPassword', 'ERROR: Your password must contain an Upper Case/Capital Letter');
			
			return false;
			}
		}	   
		
		
		$isnumber = 0;

		for($x = 0; $x < $count; $x++)
		{
			if(ctype_digit($password[$x]))
			{
				//echo "Password has a digit!";
				$isnumber = 1;
				break;
			}
			else;
		}


		if($isnumber == 0)
		{
			//echo "ERROR! Password requires at least one digit!";
			echo "<br>";
			echo "<br>";
			echo "<br>";
		
			$this->setErrors('formPassword', 'ERROR: Your password must contain a Digit');
		
			return false;
		}
		
		
		
        echo "<br>";
		//echo "Valid Password";
		echo "<br>";
		return true;
	}
	
	
	protected function isRetypeValid(string $retype) : bool
	{
		$original = $_POST['formPassword'];
		
		if(empty($retype))
		{
			//echo "ERROR! Please re-enter your Password!";
			$this->setErrors('formRetype', 'ERROR: Please re-enter your password!');
			return false;
		}
		
		if(strcmp($retype, $original) != 0)
		{
			//echo "ERROR! Passwords must match!";
			return false;
		}
		
		return true;
	}
	
	
	protected function isLoginEmailValid(string $email) : bool
	{
		if(empty($email))
		{
			//echo "ERROR! Email is required!";
			
			$this->setErrors('LoginEmail', 'ERROR: Please enter Your Email Address!');
			
			return false;
		}
		
		return true;
	}
	
	
	protected function isLoginPasswordValid(string $password) : bool
	{
		if(empty($password))
		{
			//echo "ERROR! Email is required!";
			
			$this->setErrors('LoginPassword', 'ERROR: Please enter Your Password!');
			
			return false;
		}
		
		return true;
	}
	
}