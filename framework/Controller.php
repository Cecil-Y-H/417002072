<?php

abstract class Controller
{
    protected $model = null;
    protected $view = null;


    public function setModel(ObservableModel $model)
    {
        $this->model = $model;
    }

    public function setView(View $view)
    {
        $this->view = $view;
    }

    abstract public function run();
	
	
	
	
	//Please note that these two methods are STRICTLY for testing purposes only.
	//They do not factor into the use of the framework proper.
	
	public function getModel()
	{
		return $this->model;
	}
	
	public function getView()
	{
		return $this->view;
	}
}

