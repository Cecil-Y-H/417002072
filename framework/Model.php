<?php

abstract class Model
{
    protected $cached_json = [];

    abstract public function getAll() : array;

    abstract public function getRecord(string $id): array;

    public function loadData(string $file) : array
    {
        $filename = basename($file, '.json');

        if(!isset($this->cached_json[$filename]) || empty($this->cached_json[$filename]))
        {
            $json_file = file_get_contents($file);
            $this->cached_json[$filename] = json_decode($json_file, true);
        }

        return $this->cached_json[$filename];
    }
}