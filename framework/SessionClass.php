<?php

class SessionClass
{

    //protected $access = ['profile'=>['testuser', 'John Doe']];
    
	
	//array containing the emails of the people who have been registered.
	protected $access = ['profile' => 
							[	'testuser',	
								'tester@comp3170.com',
								'EmailAddress@hotmail.com',
								'exampleaddress@gmail.com',
								'AliceWilson@gmail.com',
								'DavidSamson@yahoo.com',	
								'bobby@gmail.com'
							]
						];
	
	
	

    public static function create()
    {
        session_start();
    }

    public static function destroy()
    {
        session_destroy();
		setcookie("PHPSESSID", "", time() - 3600);
    }    

    public static function add($name, $value)
    {
        if (!preg_match('/^[a-zA-Z_\x80-\xff][a-zA-Z0-9_\x80-\xff]*$/', $name)) {
            trigger_error('Invalid variable name received', E_USER_ERROR);
        }       

        $_SESSION[$name] = $value;
    }

    public function see($name)
    {
        if(isset($_SESSION[$name]))
        {
            return $_SESSION[$name];
        }

        return null;
    }

    public function accessible($user, $page) : bool
    {
        if(in_array($user, $this->access[$page]))
        {
             return true;
        }

        return false;
    }

    public function remove(string $name)
    {
        if(isset($_SESSION[$name]))
        {
            unset($_SESSION[$name]);
        }
    }
	
	
}