<?php

Interface ObserverInterface
{
    public function update(ObservableModel $obs);
}