<?php

use PHPUnit\Framework\TestCase;

require 'framework/SessionClass.php';

class SessionClassTest extends TestCase
{
    
	
	public function testSessionObjectIsCreated() : void
    {
		$testObject = new SessionClass;
        $this->assertIsObject($testObject);
    }


	public function testSessionDestroy()
	{
		$testObject = new SessionClass;	
		$testObject->create();
		$testObject->destroy();
		$this->assertNull($testObject);
		
	}


	public function testSessionAdd()
	{
		$testObject = new SessionClass;
		$name = 'test';
		$value = 'testvalue';
		//$testObject->create();
		$testObject->add($name, $value);
		//echo $_SESSION[$name];
		$this->assertTrue($_SESSION['test'] == 'testvalue');
	}


	public function testRemove()
	{
		$testObject = new SessionClass;
		$name = 'test';
		$value = 'testvalue';
		$testObject->add($name, $value);
		$testObject->remove($name);
		$this->assertArrayNotHasKey('test', $_SESSION);
	}


	public function testAccessible()
	{
		$testObject = new SessionClass;
		$testUser = 'testuser';
		$testPage = 'profile';
		$this->assertTrue($testObject->accessible($testUser, $testPage));
		
	}	
	

	
    public function testSessionClassHasStaticMethodCreate() : void
    {
        $method = new ReflectionMethod('SessionClass', 'create');

        $this->assertTrue($method->isStatic(), 'Method create() exists but is not static');
    }    

    

	



}