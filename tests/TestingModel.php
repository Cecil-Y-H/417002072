<?php

require 'Model.php';

class TestingModel extends ObservableModel
{
	
	public function getAll() : array
	{
		return [];
	}
	
	public function getRecord(string $id) : array
	{
		return [];
	}
	
	/*
		
	public function loadData(string $file) : array
    {
        $filename = basename($file, '.json');
        if(!isset($this->cached_json[$filename]) || empty($this->cached_json[$filename]))
        {
            $json_file = file_get_contents($file);
            $this->cached_json[$filename] = json_decode($json_file, true);
        }
        return $this->cached_json[$filename];
    }


	*/
}
