<?php

use PHPUnit\Framework\TestCase;


//require 'autoload.php';
//require 'framework/View.php';

class ViewTest extends TestCase
{

		public function testViewObjectCreated()
		{
			$testObject = new View();
			$this->assertIsObject($testObject);
		}
	
		public function testSetTemplate()
		{
			$testObject = new View();
			$testObject->setTemplate(TPL_DIR . '/courses.tpl.php');
			$tplfile = $testObject->getTPL();
			$this->assertFileExists($tplfile);
		}
		
		public function testAddVar()
		{
			$testObject = new View();
			$testObject->addVar('test', 'testvalue');
			$varTest = $testObject->getData();
			$this->assertEquals('testvalue', $varTest['test']);
			
		}
		
}
