<?php

use PHPUnit\Framework\TestCase;

//require 'TestingController.php';
require 'app/IndexController.php';

class ControllerTest extends TestCase
{
	
	public function testControllerObjectCreated()
	{
		$testObject = new IndexController();
		$this->assertIsObject($testObject);
	}
	
	
	public function testSetModel()
	{
		$testObject = new IndexController();
		$testObject->run();
		$testModel = $testObject->getModel();
		$this->assertIsObject($testModel);
	}
	
	public function testSetView()
	{
		$testObject = new IndexController();
		$testObject->run();
		$testView = $testObject->getView();
		$this->assertIsObject($testView);
	}
	
}	
	
	
	



