<?php
class IndexModel extends ObservableModel
{
    
    public function getAll() : array
    {
        $data = $this->loadData(DATA_DIR . '/courses.json');

        $popular = array_column($data['courses'], 3);
        $recommended = array_column($data['courses'], 2);
        $extra = $data['courses'];

        array_multisort($recommended, SORT_DESC, $data['courses']);
        $recommended = array_slice($data['courses'], 0, 8);
        
        array_multisort($popular, SORT_DESC, $extra);
        $popular = array_slice($extra, 0, 8);

        return['popular'=>$popular, 'recommended'=>$recommended];

    }

    public function getRecord(string $id) : array
    {
        return [];
    }




}