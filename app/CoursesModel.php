<?php
class CoursesModel extends ObservableModel
{
    public function getAll(): array
    {
        $course = $this->loadData(DATA_DIR . '/courses.json');
		
		$courseconnection = $this->loadData(DATA_DIR . '/course_instructor.json');
		$lecturer = $this->loadData(DATA_DIR . '/instructors.json');
		
		$faculty = $this->loadData(DATA_DIR . '/faculty_department.json');
		$faculdeptcourse = $this->loadData(DATA_DIR . '/faculty_dept_courses.json');

		
        $coursename = array_column($course['courses'], 0);
        $courseimage = array_column($course['courses'], 4);
		
		$courselink = array_column($courseconnection['course_instructor'], 0);
		$courselecture = array_column($lecturer['instructors'], 0);
	
		$department = array_column($faculty['faculty_department'], 0);
		
		
	
		//echo "<br>";echo "<br>";
		//echo "<br>";echo "<br>";
		//echo "<br>";echo "<br>";
	
		//var_dump($department);
		
		
		$tablearray = array();
		$x = 0;
		
		
		$testobject = array_column($faculdeptcourse['faculty_dept_courses'], 0);
				
			foreach($testobject as $index => $category)
			{
				//echo "{$index} => {$category}";
				//echo "<br>";			
				$tablearray[$x][0] = $category;
				$x++;
				$index++;
			}
			echo "<br>";
		
		
		
		$x = 0;
		$testobject = array_column($faculdeptcourse['faculty_dept_courses'], 1);
				
			foreach($testobject as $index => $category)
			{
				//echo "{$index} => {$category}";
				//echo "<br>";			
				$tablearray[$x][1] = $category;
				$x++;
				$index++;
			}
			echo "<br>";
		
		$x = 0;
		$testobject = array_column($faculdeptcourse['faculty_dept_courses'], 2);
				
			foreach($testobject as $index => $category)
			{
				//echo "{$index} => {$category}";
				//echo "<br>";			
				$tablearray[$x][2] = $category;
				$x++;
				$index++;
			}
			echo "<br>";
		
		
		$x = 0;
		$testobject = array_column($faculdeptcourse['faculty_dept_courses'], 3);
				
			foreach($testobject as $index => $category)
			{
				//echo "{$index} => {$category}";
				//echo "<br>";			
				$tablearray[$x][3] = $category;
				$x++;
				$index++;
			}
			echo "<br>";
		
		
		$x = 0;
		$testobject = array_column($faculdeptcourse['faculty_dept_courses'], 4);
				
			foreach($testobject as $index => $category)
			{
				//echo "{$index} => {$category}";
				//echo "<br>";			
				$tablearray[$x][4] = $category;
				$x++;
				$index++;
			}
			echo "<br>";
		
		
		$x = 0;
		$testobject = array_column($faculdeptcourse['faculty_dept_courses'], 5);
				
			foreach($testobject as $index => $category)
			{
				//echo "{$index} => {$category}";
				//echo "<br>";			
				$tablearray[$x][5] = $category;
				$x++;
				$index++;
			}
			echo "<br>";
		
		
		
		$x = 0;
		$testobject = array_column($faculdeptcourse['faculty_dept_courses'], 6);
				
			foreach($testobject as $index => $category)
			{
				//echo "{$index} => {$category}";
				//echo "<br>";			
				$tablearray[$x][6] = $category;
				$x++;
				$index++;
			}
			echo "<br>";
		
		/*
		for($x = 0; $x < 6; $x++)
		{
			for($y = 0; $y < 7; $y++)
			{
				echo $tablearray[$x][$y];
				echo "       ";
			}
			echo "<br>";
			echo "<br>";
			
		}*/
		
		
		$CourseDepartment = array();
		$place = 0;
		
		foreach($coursename as $courseIndex => $courseName)
		{
				$courseIndex++;
			
				for($x = 0; $x < 6; $x++)
				{	
					for($y = 0; $y < 7; $y++)
					{
						if($courseIndex == $tablearray[$x][$y])
						{
							$row = $x + 1;
							//echo "{$courseIndex} => {$courseName}";
							//echo " ----------{$row} Row";
							//echo "<br>";
							
							foreach($department as $deptIndex => $deptValue)
							{
								$deptIndex++;
								if($row == $deptIndex)
								{
									//echo "{$courseName} => {$deptValue}";
									//echo "<br>";
									//echo "<br>";
									
							
									$CourseDepartment[$place][0] = $courseName;
									$CourseDepartment[$place][1] = $deptValue;
									$place++;
								}
							}
						}	
					}
				}
			}
		
		
		
		
		
		$CourseLecturer = array();
		$row = 0;
	
	
		//The following set of nested foreach loops is designed to
		//join together the arrays gotten from the 3 JSON Files
		//The end result is an array with the Course and the Lecturer assigned to it.
		//It basically replicates a traditional SQL JOIN procedure.
		
		foreach($coursename as $courseIndex => $courseName)
		{
			$courseIndex++;
			foreach($courselink as $linkIndex => $linkValue)
			{
				$linkIndex++;
				
				if($courseIndex == $linkIndex)
				{
					//echo "{$courseIndex} => {$linkIndex} => {$linkValue}";
					
					//echo "<br>";echo "<br>";
					//echo "<br>";echo "<br>";
					
					foreach($courselecture as $lecIndex => $lecName)
					{
						$lecIndex++;
						
						if($linkValue == $lecIndex)
						{
							//echo "{$courseName} => {$courseIndex} => {$linkIndex} => {$linkValue} => {$lecName}";
							//echo "<br>";echo "<br>";
							//echo "<br>";echo "<br>";
							
							$CourseLecturer[$row][0] = $courseName;
							$CourseLecturer[$row][1] = $lecName;
							
							$row++;
						}
						else;	
					}	
				}
				else;
			}
		}

		/*
		for($x = 0; $x < sizeof($CourseLecturer); $x++)
		{
			echo "{$CourseLecturer[$x][1]} teaches {$CourseLecturer[$x][0]}";
			echo "<br>";
		}

		//echo $CourseLecturer[0][0];
		//echo "<br>";
		//echo $CourseLecturer[0][1];
		*/







        return ['name'=>$coursename, 'image'=>$courseimage, 'CourseLecturer'=>$CourseLecturer, 'CourseDepartment'=>$CourseDepartment];
        //name is the array carrying the course names and is passed to the Course Controller to display the courses.
    }

    public function getRecord(string $id): array
    {
        return [];
    }
}