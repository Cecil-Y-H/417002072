<?php
class IndexController extends Controller
{
    public function run()
    {
        $view = new View();
        $view->setTemplate(TPL_DIR . '/index.tpl.php');

        $this->setModel(new IndexModel());
        $this->setView($view);


        $this->model->attach($this->view);

        $data = $this->model->getAll();

        $this->model->updateThechangedData($data);


        $this->model->notify();
    }

}