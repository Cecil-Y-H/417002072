<?php

require 'Validation.php';
require 'Session.php';
require 'framework/SessionClass.php';


class LoginController extends Controller
{
	public function run()
	{
		$view = new View();
		
		$view->setTemplate(TPL_DIR . '/login.tpl.php');
		
		$this->setModel(new LoginModel());
		$this->setView($view);
		
		$this->model->attach($this->view);
		
		$data = $this->model->getAll();
		
		$this->model->updateThechangedData($data);
		
		$this->model->notify();
		
		
		
		if(!empty($_POST))
		{
			$validator = new Validation($_POST);
			$result = $validator->loginValidate();
			
			
			if(!$result)
			{
				echo "<br>";
				echo "Please see your Errors below!\n";
				$errors = $validator->getErrors();
				$view->addVar('errors', $errors);
				$view->display();
			}
			
			else
			{
				$email = $_POST['LoginEmail'];
				//echo "<br>";
				//echo $email;
				$hash_array = $this->model->getRecord($email);
				//var_dump($hash_array);
				//echo "<br>";echo "<br>";echo "<br>";
				
				
				if($this->model->verifyPassword($hash_array))
				{
					//echo "proceeding to profile page";
					$login = new SessionClass();
					$login->create();
					$login->add('LoggedIn', $_POST['LoginEmail']);
					
	
					header('Location:profile.php');
				}
				
				else;
				
			}
			
			echo "Please enter a name and password!";
		}
	}

	
	public function logout()
	{
		$testobject = new SessionClass();
		$testobject->destroy();
		header('Location:index.php');
	}
}


