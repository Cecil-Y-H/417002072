<?php

class LoginModel extends ObservableModel
{

	public function getAll(): array
	{
		return [];
	}


	//Here getRecord retrieves the user record using the
	//Email Address entered at the Login Page as the parameter
	public function getRecord(string $id) : array
	{
		
		$array = $this->loadData(DATA_DIR . '/users.json');
		$test = $array["users"];
		
		foreach($test as $key => $value)
		{
			
			
			$compare = $value["email"];
			
			if($compare == $id)
			{
				
				$hash = $value["password"];
				
			
				return['hash' => $hash];
			}
			
			else;
			
		}
		
		return [];
	}

	
	
	public function verifyPassword(array $pass) : bool
	{
			//echo "verifyPassword Method Invoked";
			//echo "<br>";echo "<br>";
			
			//var_dump($pass);
			
			$hashed = $pass["hash"];
			$password = $_POST['LoginPassword'];
			
			//echo $hashed;
			//echo $password;
			
			if(password_verify($password, $hashed))
			{
				//echo "Password is valid!";
				//echo "<br>";echo "<br>";
				return true;
			}
			
			
			return false;
	}
}