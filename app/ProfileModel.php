<?php
class ProfileModel extends ObservableModel
{
    public function getAll(): array
    {
		return [];
    }

    public function getRecord(string $id): array
    {
		$data = $this->loadData(DATA_DIR . '/usercourses.json');
		$test = $data['users-courses'];
		
		foreach($test as $key => $value)
		{
			if($value["email"] == $id)
			{
				//echo "Matching Record Found. Displaying Courses";
				
				$user = array();
				$user[0] = $value["course1"];
				$user[1] = $value["course2"];
				$user[2] = $value["course3"];
				$user[3] = $value["course4"];
				
				/*(
					'name' => $value["name"],
					'email' => $value["email"],
					'course1' => $value["course1"],
					'course2' => $value["course2"],
					'course3' => $value["course3"],
					'course4' => $value["course4"]
				);*/
				
				
				return ['user'=>$user];
			}
			
		}
		
        return [];
    }
}