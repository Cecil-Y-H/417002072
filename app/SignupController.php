<?php

require 'Validation.php';

class SignupController extends Controller
{
    public function run()
    {
        
        $view = new View();
		
		$view->setTemplate(TPL_DIR . '/signup.tpl.php');

		$this->setModel(new SignupModel());
		$this->setView($view);

		$this->model->attach($this->view);

		$data = $this->model->getAll();

		$this->model->updateThechangedData($data);

		$this->model->notify();
		
		$valid = $_SESSION['valid'];
		
		if( (!empty($_POST)) && $valid == 1 )
		{
			
			$validator = new Validation($_POST);
			$result = $validator->signupValidate();
			
			if(!$result)
			{
				echo "<br>";
				echo "Please see your Errors below!\n";
				$errors = $validator->getErrors();
				$view->setTemplate(TPL_DIR . '/signup.tpl.php');
				$view->addVar('errors', $errors);
				$view->display();
			}
			
			else
			{
				
				$this->model->writeRecord();
				
				header('Location:login.php');
			}	
		}
		
	}
		
    

}