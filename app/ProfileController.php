<?php
class ProfileController extends Controller
{
    public function run()
    {
		// This is here due to a Notice displaying if you attempt to
		// access the Profile Page without logging in first.
		error_reporting(~E_NOTICE);
		//SessionClass::create();
		
        $sess = new SessionClass();
        $sess->create();
		//$sess->add('user', 'testuser');

        //$sess->remove('user');

        $view = new View();
        $view->setTemplate(TPL_DIR . '/profile.tpl.php');


        $this->setModel(new ProfileModel());
        $this->setView($view);

        $this->model->attach($this->view);


        $user = $sess->see('user');
		
		$userEmail = $_SESSION['LoggedIn'];
		//echo $userEmail;

        if($sess->accessible($userEmail, 'profile'))
		{
		
            //get all the courses I'm registered for
			//Using the email from the login page as an ID
            $data = $this->model->getRecord($userEmail);

            //Tells the model to update the changed data
            $this->model->updateThechangedData($data);

            //tell the model to contact its observers
            $this->model->notify();

            
        }

        else
        {
			
            $view->setTemplate(TPL_DIR . '/login.tpl.php');
            $view->display();
        }

    }

}