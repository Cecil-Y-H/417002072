<?php
require 'framework/SessionClass.php';
class CoursesController extends Controller
{
    public function run()
    {
		// This is here due to a Notice displaying if you attempt to
		// access the Courses Page without logging in first.
		
		error_reporting(~E_NOTICE);
        SessionClass::create();

        $sess = new SessionClass();
      

        $view = new View();
        $view->setTemplate(TPL_DIR . '/courses.tpl.php');


        $this->setModel(new CoursesModel());
        $this->setView($view);

        $this->model->attach($this->view);


        $user = $sess->see('user');

		
		$userEmail = $_SESSION['LoggedIn'];
		

        if($sess->accessible($userEmail, 'profile'))
        {
            //get all the courses from the json file
            $data = $this->model->getAll();

            //Tells the model to update the changed data
            $this->model->updateThechangedData($data);

            //tell the model to contact its observers
            $this->model->notify();

            
        }

        else
        {
            $view->setTemplate(TPL_DIR . '/login.tpl.php');
            $view->display();
        }

    }

}