<?php
class SignupModel extends ObservableModel
{
    public function getAll(): array
    {
        return [];
    }

    public function getRecord(string $id): array
    {
        return [];
    }
	
	//This function is used to update/append the JSON File
	//that 
	
	public function writeRecord()
	{
		$fullname = $_POST['formFullName'];
		$email = $_POST['formEmail'];
		$password = $_POST['formPassword'];
				
		$hash = password_hash($password, PASSWORD_DEFAULT);

		//loadData(DATA_DIR . '/users.json');
		
		if(file_exists(DATA_DIR . '/users.json'))
		{
			
			$arrayData = $this->loadData(DATA_DIR . '/users.json');
			
			$extra = array(
				'name' => $fullname,
				'email' => $email,
				'password' => $hash
			);
			
			array_push($arrayData['users'], $extra);
			
			$result = json_encode($arrayData);
			
			
			if(file_put_contents(DATA_DIR . '/users.json', $result))
			{
				echo "File was successfully Appended.";
			}
			
			else
			{
				echo "FILE NOT APPENDED!";
			}
		}
		
		else
		{
			echo "NO SUCH FILE EXISTS!";
		}
		
	}
}